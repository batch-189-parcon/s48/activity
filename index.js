//Mock Database
// Empty since hasnt created post yet
// Refresh page gets empty value
let posts = [];

//ID number for every post
let count = 1;

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (event) => {

	//To prevent page from loading
	event.preventDefault()

	//add items in empty array
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++;
	//console.log(count) can indicate other values

	showPosts(posts)
	alert('Successfully added.') 

})

const showPosts = (posts) => {
	let postEntries = '';

	//forEach check all elements in array
	//(post) represent the element in array
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}" > 
				<h3 id="post-title-${post.id}"> ${post.title}</h3>
				<p id="post-body-${post.id}"> ${post.body} </p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>

		`
	})

	//Created post will be saved inside the div tag
	document.querySelector("#div-post-entries").innerHTML = postEntries;


}

//Edit Post

const editPost = (id) => {

	//innerHTML nsa loob na laman
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

//Update Post
//click 'submit' to process
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {

	event.preventDefault();

	for (let i = 0; i < posts.length;  i++) {

		//since ID is number and need to change to string
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts)
			alert("Successfully updated");

			break;
		}
	}

})


//Delete Post
// const deletePost = document.getElementById(`#post-${id}`);
// deletePost.remove();


const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove()
	delete posts(id)
}